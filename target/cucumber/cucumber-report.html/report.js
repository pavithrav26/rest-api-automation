$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/MyAPITests.feature");
formatter.feature({
  "name": "RestAPI validation with Get and Post",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "RestAPI Validation",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@test"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the restAPI is invoked for POST with test data",
  "keyword": "When "
});
formatter.match({
  "location": "restAPIStepDefinition.the_restAPI_is_invoked_for_POST_with_test_data(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the request is submitted",
  "keyword": "And "
});
formatter.match({
  "location": "restAPIStepDefinition.the_request_is_submitted()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Validate the response with the expected json",
  "keyword": "Then "
});
formatter.match({
  "location": "restAPIStepDefinition.validate_the_response_with_the_expected_json()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "RestAPI Get Validation",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@test"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the restAPI is invoked for GET with test data for \"userid 1\"",
  "keyword": "When "
});
formatter.match({
  "location": "restAPIStepDefinition.the_restAPI_is_invoked_for_POST_with_test_data(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the request is submitted",
  "keyword": "And "
});
formatter.match({
  "location": "restAPIStepDefinition.the_request_is_submitted()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Validate the response with the expected json",
  "keyword": "Then "
});
formatter.match({
  "location": "restAPIStepDefinition.validate_the_response_with_the_expected_json()"
});
formatter.result({
  "status": "passed"
});
});