package runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty", "html:target/cucumber/cucumber-report.html",
		"json:target/cucumber/cucumber-report.json" }, glue = { "stepDefinitions" }, features = {
				"src/test/resources/features" }, tags = { "@test" }, monochrome = true)

public class cucumberRunnerTest {

}
