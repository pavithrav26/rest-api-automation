package stepDefinitions;

import org.json.simple.parser.ParseException;
import org.skyscreamer.jsonassert.JSONAssert;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import io.cucumber.core.api.Scenario;
import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utils.TestFixtures;
import utils.utilitymethods;

public class restAPIStepDefinition {
	Scenario scenario;
	TestFixtures testfixtures = new TestFixtures();

	/**
	 * This assigns current scenario to scenario object.
	 * 
	 * @param scenario
	 */
	@Before
	public void before(Scenario scenario) {
		this.scenario = scenario;
	}

	/**
	 * This methods allows to store the request and expected response into the POJOs
	 * and then use for submit and validation.
	 * 
	 * @param apiType    - either GET or POST
	 * @param getAPIdata - only for GET requests with query params
	 * @throws UnirestException -
	 * @throws ParseException
	 */
	@When("the restAPI is invoked for (.*) with test data(?: for \"(.*)\"|)")
	public void the_restAPI_is_invoked_for_POST_with_test_data(String apiType, String getAPIdata)
			throws ParseException {
		try {
			String strResponse;
			testfixtures.setApiType(apiType);
			String scenarioName = scenario.getName().replaceAll(" ", "");

			if (apiType.trim().equalsIgnoreCase("post")) {

				String strRequest = utilitymethods
						.getFileContentsAsString("src/test/resources/data/" + scenarioName + "/request.json");
				testfixtures.setRequest(strRequest);

//			testfixtures.setRequestJSON(utilitymethods.convertStringToJSON(testfixtures.getRequest()));
//			testfixtures.setResponseJSON(utilitymethods.convertStringToJSON(testfixtures.getResponse()));

			} else if (apiType.trim().equalsIgnoreCase("get")) {

				String queryParams = utilitymethods.buildAPIparams(getAPIdata);
				testfixtures.setQueryParams(queryParams);

			} else {
				System.out.println("worng api type in feature");
			}

			strResponse = utilitymethods
					.getFileContentsAsString("src/test/resources/data/" + scenarioName + "/response.json");
			testfixtures.setResponse(strResponse);
		} catch (Exception e) {
			System.out.println("Exception:" + e);

		}
	}

	/**
	 * The below methods submits the request to the API with different http methods
	 * like GET and POST
	 * 
	 * @throws Throwable
	 */
	@When("^the request is submitted$")
	public void the_request_is_submitted() throws Throwable {
		HttpResponse<JsonNode> actualResponse;
		if (testfixtures.getApiType().equalsIgnoreCase("post")) {
			actualResponse = Unirest.post("https://jsonplaceholder.typicode.com/todos/1/posts")
					.header("Content-Type", "application/json").body(testfixtures.getRequest()).asJson();
		} else {
			actualResponse = Unirest
					.get("https://jsonplaceholder.typicode.com/todos/1/posts" + testfixtures.getQueryParams())
					.header("Content-Type", "application/json").asJson();
		}

		System.out.println(actualResponse.getBody().toString());

		testfixtures.setActualResponse(actualResponse.getBody().toString());

	}

	/**
	 * The below method validates the expected response to the actual response from
	 * API
	 * 
	 * @throws Throwable
	 */
	@Then("^Validate the response with the expected json$")
	public void validate_the_response_with_the_expected_json() throws Throwable {

		JSONAssert.assertEquals(testfixtures.getResponse(), testfixtures.getActualResponse(), true);

	}

}
