Feature: RestAPI validation with Get and Post
@test
  Scenario: RestAPI Validation
    When the restAPI is invoked for POST with test data
    And the request is submitted
    Then Validate the response with the expected json
@test
    Scenario: RestAPI Get Validation
    When the restAPI is invoked for GET with test data for "userid 1"
    And the request is submitted
    Then Validate the response with the expected json